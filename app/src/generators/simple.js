// function* symplegenerator() {
//     let ask1 = yield '2 + 2?'
//
//     yield ask1
// }
//
// const mygen = symplegenerator()
//
// console.log(mygen.next())
// console.log(mygen.next(4))

function* asyncGenerator() {
    let testDataFetch = yield fetch('https://reqres.in/api/users?page=2')

    let info = yield testDataFetch.json();

    return info
}

function execute(generator, yieldValue) {

    let next = generator.next(yieldValue);
    console.log('')

    if (!next.done) {
        next.value.then(
            result => execute(generator, result),
            err => generator.throw(err)
        );
    } else {
        // обработаем результат return из генератора
        // обычно здесь вызов callback или что-то в этом духе
        console.log(next.value)
    }

}

execute(asyncGenerator())

