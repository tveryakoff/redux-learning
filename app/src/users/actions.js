export const ADD_USER = 'ADD_USER'
export const DEL_USER = 'DEL_USER'

export const addUser = (user) => ({
    type: ADD_USER,
    payload: {user}
})

export const delUser = (index) => ({
    type: DEL_USER,
    payload: {index}
})
