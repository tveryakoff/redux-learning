import React from 'react'
import {connect} from 'react-redux'
import {loadUserList} from './loadUserList'


class UserList extends React.Component {

    componentDidMount() {
        this.props.loadUserList()
    }

    render() {

        console.log('list', this.props.userList)

        const {userList} = this.props

        if (!Array.isArray(userList) || userList.length === 0) {
            return (
                <div>no users found</div>
            )
        }

        return (
            <div>
                <header>user list:</header>
                <div>
                    {userList.map(user => (
                        <div key={user.id}>{`${user && user.last_name} ${user && user.first_name}`}</div>
                    ))}
                </div>
            </div>
    )
    }
}

const mapStateToProps = (state) => ({
    userList: state.userList
})

const mapDispatchToProps = {
    loadUserList
}

export default connect(mapStateToProps, mapDispatchToProps)(UserList)


