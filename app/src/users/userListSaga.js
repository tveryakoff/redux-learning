import { takeEvery, call, put } from 'redux-saga/effects'

export function* fetchUserList () {
    try {

        const userList = yield call(() => {
                return fetch('https://reqres.in/api/users?page=2')
                    .then(res => res.json())
            }
        );

        yield  put({ type: 'USER_LIST_LOAD_SUCCESS', payload: {userList} })
    }
    
    catch (error) {
        yield put({ type: 'USER_LIST_LOAD_ERROR', payload: {error} })
    }
}

export function* watchFetchUserList() {
    yield takeEvery('USER_LIST_REQUESTED', fetchUserList)
}

