export const logger = ({dispatch, getState}) => next => action => {
    console.log('current state', getState())
    console.log('action', action)

    const result = next(action)

    console.log('next state', getState())
    return result
}