import React from 'react';
import './App.css';
import {Provider} from 'react-redux'
import UserList from './users/UserList'

function App(props) {
  const {store} = props
  return (
      <Provider store={store}>
        <div className="App">
          <UserList/>
        </div>
      </Provider>
  );
}

export default App;
