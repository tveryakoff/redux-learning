const minus1 = num => num - 1
const minus2 = num => num - 2

export const compose = funcs => funcs.reduce((a, b) => (...args) => a(b(...args)))

const composedMinus = compose([minus1,minus2])

console.log(composedMinus(4))
