
const initialStateUsers = {
    isLoading: false,
    userList: [],
    error: null,
}

export const users = (state=initialStateUsers, action) => {
    switch (action.type) {
        case 'USER_LIST_REQUESTED':
            return {
                ...state,
                isLoading: true,
                error: initialStateUsers.error,
            }

        case 'USER_LIST_LOAD_SUCCESS':
            return {
                ...state,
                userList: action.payload.userList.data,
                isLoading: false,
            }

        case 'USER_LIST_LOAD_ERROR':
            return {
                ...state,
                isLoading: false,
                error: action.payload.error
            }

        default:
            return state

    }
}