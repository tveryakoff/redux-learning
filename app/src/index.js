import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { createStore, applyMiddleware } from 'redux'
import {users} from './reducers'
import './compose'
import {logger} from './reduxMidlewares/logger'
import './generators/simple'
import createSagaMiddleware from 'redux-saga'
import {watchFetchUserList} from './users/userListSaga'

const sagaMidlleware = createSagaMiddleware()

const store = createStore(users,{users: []}, applyMiddleware(logger, sagaMidlleware))

sagaMidlleware.run(watchFetchUserList)




ReactDOM.render(<App store={store} />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
