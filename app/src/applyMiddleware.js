import {compose} from './compose'

export const applyMiddleware = (...middlewares) => createStore => (...args) => {
    const store = createStore(...args)

    const middlewareApi = {
        getState: store.getState,
        dispatch: store.dispatch
    }

    const chain = middlewares.map(middleware => middleware(middlewareApi))

    const dispatch = compose(chain)(store.dispatch)

    return {
        ...store,
        dispatch
    }
}